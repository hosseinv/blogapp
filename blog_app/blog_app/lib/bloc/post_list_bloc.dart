import 'package:blog_app/core/master/master_event.dart';
import 'package:blog_app/core/master/master_state.dart';
import 'package:blog_app/core/util.dart';
import 'package:blog_app/model/post_list_model.dart';
import 'package:blog_app/repository/post_list_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'dart:async';

part 'post_list_event.dart';
part 'post_list_state.dart';

class PostListBloc extends Bloc<MasterEvent, MasterState>{
  PostListBloc() : super(PostListInitial());

  int _page = 1;
  String res;
  List<PostModel> postModel;
  bool isLoadMoreError = false,_isFetching = false,_isLoadMore,isSearch = false;

  @override
  Stream<MasterState> mapEventToState(MasterEvent event)
  async*{
    if(event is GetPostEvent){
      try {
        yield RequestLoadingState(event: event);

        isSearch = false;
        postModel = await PostListRepository().getPost(_page);

        _isLoadMore = postModel.length == 10;

        yield RequestResponseState(event: event);
      }catch(e){
        isSearch = false;
        yield RequestErrorState(error: e.toString(),event: event);
      }
    }
    else if (event is LoadMoreEvent) {
      if (!_isFetching && _isLoadMore) {
        try {
          _isFetching = true;
          yield RequestLoadingState(event: event);

          var res =
          await PostListRepository().getPost(++_page);
          var list = res;
          postModel.addAll(list);

          _isLoadMore = list.length == 10;
          _isFetching = false;
          isLoadMoreError = false;

          yield RequestResponseState(event: event);
        } catch (e) {
          _isFetching = false;
          isLoadMoreError = true;
          _page--;
          yield RequestErrorState(
              error: Utils().mapExceptions(e), event: event);
        }
      }
    } else if (event is AddCommentPostEvent) {
      try {
        yield RequestLoadingState(event: event);

        res = await PostListRepository().addCommentPost(event.id, event.user, event.body);

        Utils().showToast(res, event.context);
        yield UpdateCommentState();
      } catch (e) {
        yield RequestErrorState(
            error: Utils().mapExceptions(e), event: event);
      }

    }else if (event is SearchPostEvent) {
      try {
        yield RequestLoadingState(event: event);

        isSearch = true;
        postModel.clear();
        postModel = await PostListRepository().searchPost(event.search);

        yield RequestResponseState(event: event);
      } catch (e) {
        print(e.toString());
        isSearch = false;
        yield RequestErrorState(
            error: Utils().mapExceptions(e), event: event);
      }
    }else if(event is ResetEvent){
      yield ResetState();
    }
  }

  void reset() {
    postModel.clear();
    isLoadMoreError = false;
    _isFetching = false;
    _isLoadMore = true;
    isSearch = false;
    _page = 1;
  }
}