part of 'post_list_bloc.dart';

class GetPostEvent extends MasterEvent {}

class LoadMoreEvent extends MasterEvent {}

class ResetEvent extends MasterEvent {}

class UpdateCommentEvent extends MasterEvent {}

class AddCommentPostEvent extends MasterEvent {
  final String user, body, id;
  final BuildContext context;

  AddCommentPostEvent({this.context,this.id, this.user, this.body});
}

class SearchPostEvent extends MasterEvent {
  final String search;

  SearchPostEvent({this.search});
}
