import 'package:blog_app/core/dio/dio_default.dart';
import 'package:blog_app/model/post_list_model.dart';

class PostListRepository{
  PostListRepository._internal();
  static final PostListRepository _singleton = PostListRepository._internal();
  factory PostListRepository() => _singleton;

  Future<dynamic> getPost(int page){
    return DioDefault().request<dynamic>("/api/v1/post", RequestType.Get,
      queryParameters: {
        'page': page,
        'limit': 10,
      },);
  }

  Future<String> addCommentPost(String id,String username,String body){
    return DioDefault().request<String>("/api/v1/post/$id/comment", RequestType.Post,data: {
      'user': username,
      'body': body,
    },);
  }

  Future<dynamic> searchPost(String title){
    return DioDefault().request<dynamic>("/api/v1/post", RequestType.Get,queryParameters: {
      'title': title,
    },);
  }

}