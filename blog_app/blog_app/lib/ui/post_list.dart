import 'package:blog_app/bloc/post_list_bloc.dart';
import 'package:blog_app/core/master/master_state.dart';
import 'package:blog_app/core/res_string.dart';
import 'package:blog_app/core/util.dart';
import 'package:blog_app/core/widget/error_widget.dart';
import 'package:blog_app/core/widget/loading_widget.dart';
import 'package:blog_app/model/post_list_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PostList extends StatefulWidget {
  @override
  _PostListState createState() => _PostListState();
}

class _PostListState extends State<PostList> {
  PostListBloc _postListBloc;

  @override
  void initState() {
    _postListBloc = BlocProvider.of<PostListBloc>(context);
    _postListBloc.add(GetPostEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            ResString.postlistTitle,
            style: TextStyle(fontSize: 25, color: Colors.white),
          ),
        ),
        body: Column(children: [
          _createSearchView(),
          SizedBox(
            height: 15,
          ),
          Flexible(
              child: BlocBuilder<PostListBloc, MasterState>(
                  buildWhen: (_, state) =>
                      state.event is GetPostEvent ||
                      state.event is SearchPostEvent ||
                      (state.event is LoadMoreEvent &&
                          state is RequestResponseState) ||
                          (state.event is AddCommentPostEvent &&
                          state is UpdateCommentState),
                  builder: (context, state) {
                    if (state is RequestLoadingState) {
                      return LoadingWidget();
                    } else if (state is RequestErrorState) {
                      return ErrorsWidget(
                        error: state.error,
                      );
                    } else if (state is RequestResponseState) {
                      return NotificationListener<ScrollEndNotification>(
                          onNotification: _postListBloc.postModel.isEmpty
                              ? null
                              : _onNotification,
                          child: _postListBuilder());
                    } else {
                      return Container();
                    }
                  }))
        ]));
  }

  Widget _postListBuilder() {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: _postListBloc.postModel.length,
      itemBuilder: (context, i) {
        return _postItem(_postListBloc.postModel[i]);
      },
    );
  }

  Widget _createSearchView() {
    return new Container(
      decoration:
          BoxDecoration(border: Border.all(width: 0.5, color: Colors.grey)),
      margin: EdgeInsets.only(top: 10,right: 10,left: 10),
      child: TextField(
        onChanged: (text) {
          if (text.length == 0) {
            _postListBloc.reset();
            _postListBloc.add(GetPostEvent());
          } else {
            if (text.length >= 2) {
              _postListBloc.add(SearchPostEvent(search: text));
            }
          }
        },
        decoration: InputDecoration(
          hintText: "Search",
          fillColor: Colors.transparent,
          focusColor: Colors.transparent,
          hintStyle: new TextStyle(color: Colors.grey[500]),
        ),
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget _postItem(PostModel postModel) {
    return Card(
        semanticContainer: true,
        clipBehavior: Clip.antiAliasWithSaveLayer,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        elevation: 5,
        margin: EdgeInsets.all(10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.blueAccent,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10.0),
                    topRight: Radius.circular(10.0)),
              ),
              child: Padding(
                  padding: EdgeInsets.all(10),
                  child: Center(
                      child: Text(
                    postModel.title,
                    style: TextStyle(
                        fontSize: 20,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  ))),
            ),
            Padding(
              padding: EdgeInsets.only(top: 10, left: 10),
              child: Text(
                ResString.postComment +
                    "(" +
                    postModel.comments.length.toString() +
                    ") :",
                style: TextStyle(
                    fontSize: 15,
                    color: Colors.grey,
                    fontWeight: FontWeight.bold),
              ),
            ),
            Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: _commentList( postModel))
          ],
        ));
  }

  List<Widget> _commentList(PostModel postModel) {
    List<Widget> array = [];

    for (int i = 0; i < postModel.comments.length; i++) {
      array.add(_commentItem(postModel.comments[i]));
    }

    array.add(Container(
      width: double.infinity,
      height: 0.5,
      margin: EdgeInsets.only(top: 5),
      color: Colors.grey,
    ));

    array.add(_addComment(postModel));

    return array;
  }

  Widget _commentItem(Comment comment) {
    return Padding(
        padding: EdgeInsets.only(top: 10, left: 10, bottom: 5),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              comment.user + " : ",
              style: TextStyle(
                  fontSize: 12,
                  color: Colors.black.withAlpha(500),
                  fontWeight: FontWeight.bold),
            ),
            Flexible(
                child: Text(
              comment.body,
              style: TextStyle(
                  fontSize: 14,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            )),
          ],
        ));
  }

  Widget _addComment(PostModel postModel) {
    TextEditingController _editingControllerUsername = TextEditingController();
    TextEditingController _editingControllerBody = TextEditingController();

    return Container(
        width: double.infinity,
        margin: EdgeInsets.only(top: 5),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              bottomRight: Radius.circular(10.0),
              bottomLeft: Radius.circular(10.0)),
        ),
        child: Padding(
          padding: EdgeInsets.all(8),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: 110,
                child: TextFormField(
                  controller: _editingControllerUsername,
                  validator: (val) {
                    if (val.length == 0) {
                      return "Please enter username";
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.text,
                  autofocus: false,
                  maxLines: 1,
                  decoration: InputDecoration(
                      border: UnderlineInputBorder(),
                      fillColor: Colors.white,
                      focusColor: Colors.white,
                      hintText: 'Username',
                      hintStyle: TextStyle(fontSize: 15, color: Colors.grey)),
                ),
              ),
              Container(
                width: 0.5,
                height: 50,
                margin: EdgeInsets.only(left: 8, right: 8),
                color: Colors.grey,
              ),
              Container(
                width: 180,
                child: TextFormField(
                  controller: _editingControllerBody,
                  validator: (val) {
                    if (val.length == 0) {
                      return "Please enter body";
                    } else {
                      return null;
                    }
                  },
                  autofocus: false,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                      border: UnderlineInputBorder(),
                      fillColor: Colors.white,
                      focusColor: Colors.white,
                      hintText: 'Body',
                      hintStyle: TextStyle(fontSize: 15, color: Colors.grey)),
                ),
              ),
              Container(
                width: 0.5,
                height: 50,
                margin: EdgeInsets.only(left: 8, right: 8),
                color: Colors.grey,
              ),
              Container(
                width: 40,
                child: FlatButton(
                    padding: EdgeInsets.all(0),
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    onPressed: () {
                      if (_editingControllerUsername.text.trim().isEmpty) {
                        Utils()
                            .showToast('Please enter username', this.context);
                      } else if (_editingControllerBody.text.trim().isEmpty) {
                        Utils().showToast('Please enter body', this.context);
                      } else {
                        _postListBloc.add(AddCommentPostEvent(
                          context: context,
                            id: postModel.id,
                            user: _editingControllerUsername.text.trim(),
                            body: _editingControllerBody.text.trim()));
                      }
                    },
                    child:Icon(Icons.send, color: Colors.blue),
              ))
            ],
          ),
        ));
  }

  bool _onNotification(noti) {
    if (!_postListBloc.isLoadMoreError &&
        !_postListBloc.isSearch &&
        noti.metrics.pixels >= noti.metrics.maxScrollExtent - 30) {
      _postListBloc.add(LoadMoreEvent());
    }
    return true;
  }
}
