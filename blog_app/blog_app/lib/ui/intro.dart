import 'package:blog_app/bloc/post_list_bloc.dart';
import 'package:blog_app/core/FadeRoute.dart';
import 'package:blog_app/core/res_string.dart';
import 'package:blog_app/ui/post_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Intro extends StatefulWidget {
  @override
  _IntroState createState() => _IntroState();
}

class _IntroState extends State<Intro> {

  @override
  void initState() {

    Future.delayed(Duration(milliseconds: 1500),(){
      Navigator.of(context).pushReplacement(FadeRoute(      BlocProvider(
        create: (_) => PostListBloc(),
        child: PostList(),
      )));

    });

    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Image.asset("assets/img/main.png",width: 100,height: 100,),
            SizedBox(height: 20,),
            Text(ResString.appName,style: TextStyle(color: Colors.black,fontSize: 15),)
          ],
        ),
      )
    );
  }
}


