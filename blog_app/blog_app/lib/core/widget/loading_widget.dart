import 'package:flutter/material.dart';

class LoadingWidget extends StatelessWidget {
  LoadingWidget({this.height, this.strokeWidth, this.width});

  final double height, width, strokeWidth;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height ?? MediaQuery.of(context).size.height - (kToolbarHeight * 2),
      child: Center(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation(Colors.blue),
          strokeWidth: strokeWidth ?? 4,
        ),
      ),
    );
  }
}
