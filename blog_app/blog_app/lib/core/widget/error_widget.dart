import 'package:flutter/material.dart'
    show
        StatelessWidget,
        required,
        VoidCallback,
        Widget,
        BuildContext,
        Center,
        Column,
        MainAxisAlignment,
        Text,
        IconButton,
        Icon,
        Icons;
import 'package:flutter/material.dart';

class ErrorsWidget extends StatelessWidget {
  ErrorsWidget(
      {@required this.error, this.height, this.textColor});

  final String error;
  final double height;
  final Color textColor;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height:
          height ?? MediaQuery.of(context).size.height - (kToolbarHeight * 2),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Text(
                error,
                style: TextStyle(color: textColor ?? Colors.grey[600]),
                maxLines: 1,
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
