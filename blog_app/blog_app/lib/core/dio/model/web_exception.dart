import 'package:dio/dio.dart';

import '../../res_string.dart';

class WebApiException implements DioError {
  WebApiException({this.serverMessage, this.status});

  String serverMessage;
  int status;

  @override
  var error = WebApiException;

  @override
  RequestOptions request;

  @override
  Response response;

  @override
  DioErrorType type;

  @override
  String get message => serverMessage == null || serverMessage.isEmpty
      ? ResString.webApiException
      : serverMessage;

  @override
  RequestOptions requestOptions;

  @override
  StackTrace stackTrace;
}
