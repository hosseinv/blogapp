import 'package:dio/dio.dart';

class NoNetworkException implements DioError {
  @override
  var error = NoNetworkException;

  @override
  Response response;

  @override
  DioErrorType type;

  @override
  String get message => '';

  @override
  RequestOptions requestOptions;

  @override
  StackTrace stackTrace;
}
