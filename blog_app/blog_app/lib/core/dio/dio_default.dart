import 'package:blog_app/model/post_list_model.dart';
import 'package:dio/dio.dart';

import '../util.dart';
import 'model/network_exception.dart';
import 'model/web_exception.dart';

enum RequestType { Post, Get, Put, Delete }
enum BaseUrlType { Live, Test, Archived }

const String BASE_URL = 'https://607a9689bd56a60017ba2d1f.mockapi.io';

class DioDefault {
  DioDefault._internal() {
    _init();
  }

  static final DioDefault _singleton = DioDefault._internal();
  Dio _dio;

  factory DioDefault() {
    return _singleton;
  }

  BaseUrlType baseUrlType = BaseUrlType.Live;

  set baseUrl(String baseUrl) => _dio.options.baseUrl = baseUrl;

  get baseUrl => _dio.options.baseUrl;

  void _init() async {
    _dio = Dio();
    _dio.options.connectTimeout = 30000;
    _dio.options.receiveTimeout = 30000;
    _dio.options.baseUrl = BASE_URL;
    _dio.interceptors.add(
      InterceptorsWrapper(
        onRequest: (options, handler) async {
          print('send request：path:${options.path}，baseURL:${options.baseUrl}');
          _dio.interceptors.requestLock.lock();
          if (_dio.options.baseUrl == null) {
            _dio.options.baseUrl = BASE_URL;
          }
          if (options.data == null) {
            options.data = <String, dynamic>{};
          }

          await Utils().checkConnectivity.then((connectivity) {
            if (connectivity) {
              return handler.next(options);
            } else {
              throw NoNetworkException();
            }
          }).whenComplete(() => _dio.interceptors.requestLock.unlock());
        },
        onResponse: (Response response, handler) {
          print('recived data from server: ${response.data}');
          return handler.next(response);
        },
        onError: (DioError e, handler) {
          print(e);
          // return  handler.next(e);
          if (e is NoNetworkException) {
            throw e;
          } else {
            throw WebApiException(
              serverMessage:
                  e?.response?.data == null ? null : e.response.data['message'],
            );
          }
        },
      ),
    );
  }

  Future<T> request<T>(String api, RequestType requestType,
      {dynamic data,
      Map<String, dynamic> queryParameters,
      int retry = 0}) async {
    try {
      Response response;
      switch (requestType) {
        case RequestType.Post:
          response = await _post(api, data);
          break;
        case RequestType.Get:
          response = await _get(api, queryParameters);
          break;
        case RequestType.Put:
          response = await _put(api, data);
          break;
        case RequestType.Delete:
          response = await _delete(api);
          break;
      }
      return _fromJson<T>(response.data);
    } catch (error) {
      throw error;
    }
  }

  Future<Response> _get(String api, [queryParameters]) async {
    return await _dio.get(api, queryParameters: queryParameters);
  }

  Future<Response> _post(String api, dynamic data) async {
    return await _dio.post(api, data: data);
  }

  Future<Response> _put(String api, dynamic data) async {
    return await _dio.put(api, data: data);
  }

  Future<Response> _delete(String api) async {
    return await _dio.delete(api);
  }

  T _fromJson<T>(dynamic data) {
    if (T == String) {
      return data as T;
    }  else if (T == dynamic) {
      return (data as List).map((x) => PostModel.fromJson(x)).toList() as T;
    }
    else {
      throw Exception("Unknown class");
    }
  }
}
