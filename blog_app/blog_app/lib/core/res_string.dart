class ResString {
  static final String noNetworkAvailable = 'No internet connection!';
  static final String webApiException = 'There is something wrong! please try again.';
  static final String postlistTitle = 'Blog';
  static final String appName = 'BlogApp';
  static final String postComment = 'comment ';
  static final String postTitle = 'title : ';
}