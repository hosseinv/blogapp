
import 'dart:io';

import 'package:blog_app/core/res_string.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';

import 'dio/model/network_exception.dart';
import 'dio/model/web_exception.dart';


class Utils {
  static final Utils _singleton = Utils._internal();

  factory Utils() => _singleton;

  Utils._internal();

  Future<bool> get checkConnectivity async {
    if (kIsWeb) return true;
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      }
    } on SocketException catch (_) {
      return false;
    }
    return false;
  }

  String mapExceptions(dynamic e) {
    if (e is NoNetworkException) {
      return ResString.noNetworkAvailable;
    } else if (e is WebApiException) {
      return e.serverMessage ?? ResString.webApiException;
    } else {
      return ResString.webApiException;
    }
  }

  void showToast(String message, BuildContext context) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(message,style: TextStyle(color: Colors.white,fontSize: 15),),
    ));
  }
}