import 'package:meta/meta.dart';

import 'master_event.dart';

@immutable
abstract class MasterState {
  MasterState({this.event});

  final MasterEvent event;
}

class RequestLoadingState extends MasterState {
  RequestLoadingState({MasterEvent event}) : super(event: event);
}

class RequestErrorState extends MasterState {
  RequestErrorState({this.error, MasterEvent event}) : super(event: event);
  final String error;
}

class RequestResponseState extends MasterState {
  RequestResponseState({this.data, MasterEvent event}) : super(event: event);
  final dynamic data;
}

class ResetState extends MasterState {}
