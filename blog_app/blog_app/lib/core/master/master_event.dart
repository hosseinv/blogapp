import 'package:meta/meta.dart';

@immutable
abstract class MasterEvent {}

class ResetPageEvent extends MasterEvent {}
