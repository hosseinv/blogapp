
class PostModel {
    List<Comment> comments;
    String createdAt;
    String id;
    bool published;
    String title;
    int views;

    PostModel({this.comments, this.createdAt, this.id, this.published, this.title, this.views});

    factory PostModel.fromJson(Map<String, dynamic> json) {
        return PostModel(
            comments: json['comments'] != null ? (json['comments'] as List).map((i) => Comment.fromJson(i)).toList() : null,
            createdAt: json['createdAt'],
            id: json['id'],
            published: json['published'],
            title: json['title'],
            views: json['views'],
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['createdAt'] = this.createdAt;
        data['id'] = this.id;
        data['published'] = this.published;
        data['title'] = this.title;
        data['views'] = this.views;
        if (this.comments != null) {
            data['comments'] = this.comments.map((v) => v.toJson()).toList();
        }
        return data;
    }
}

class Comment {
    String body;
    String createdAt;
    String id;
    String postId;
    String user;

    Comment({this.body, this.createdAt, this.id, this.postId, this.user});

    factory Comment.fromJson(Map<String, dynamic> json) {
        return Comment(
            body: json['body'],
            createdAt: json['createdAt'],
            id: json['id'],
            postId: json['postId'],
            user: json['user'],
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['body'] = this.body;
        data['createdAt'] = this.createdAt;
        data['id'] = this.id;
        data['postId'] = this.postId;
        data['user'] = this.user;
        return data;
    }
}